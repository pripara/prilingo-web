#pages/urls.py
from django.urls import path
from .views import homePageView

urlpatterns = [
    #Python regular expression: 
    # user requests homepage by empty string,
    # then use homePageView.
    path('', homePageView,
name = 'home')
]